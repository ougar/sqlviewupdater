# SQLViewUpdater

## Problem

I have a football homepage, with a lot of data about the Danish Superliga.
I would like to use a lot of views, to ensure, that data in the database
is always updated. The amount of data just means, that views are out of
the question, since they are just too slow. Using tables means, that I
need to update these tables, whenever the database is updated.

The data is updated very rarely - only after a game is played (or an
error is fixed), so after these updated, I need to somehow update a 
lot of tables, and update them in the correct order.

## Solution

One solution here is to use materialized views, but MariaDB don't support
this.

To fix this, I use a "home-made" materialized views solution, where I
create a view for the table X, called X\_view. When I then need to
update the X table, I can simply insert the need rows from X\_view.
This makes the update process pretty simple, but since there are a
lot of tables, I need to be able to automatically run through the
tables, and run the update.

Some tables are simply updated by truncating the original table and
then insert all rows from the view:
```
  truncate table X;
  insert into X select * from X_view
```

Other tables are very big, and take a long time to regenerate, so in
stead I run:
```
  delete from table X where season=Y;
  insert into X select * from X_view where season=Y;
```

Again, I need a way to store this information as well, so I know how to
update which tables and in which order.

This repo has the scripts I need to manage this. These scripts are then
run from my Jenkins instance, when I manually choose to perform an update
or when an automatic update is triggered.

I could use SQL trigger as well, but usually there are a lot of updates
over a period of time, and there is no reason to run an update, before
all changes have been made.

## Files

Todo

## Install

Todo

## Usage

Todo

## Author

Kristian Hougaard (https://twitter.com/kghougaard)

Git https://gitlab.com/ougar/sqlviewupdater
